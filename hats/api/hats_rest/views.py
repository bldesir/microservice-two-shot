from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from hats_rest.models import Hat, LocationVO

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        # "id",
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number"
    ]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["style_name",
                  "color",
                  "fabric",
                  "picture",
                  "location"
                ]
    encoders = {"location": LocationVODetailEncoder()}

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "style_name"]

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatDetailEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = f"/api/locations/{content['location']}/"
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hat(request, pk):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Hat does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            hat = Hat.objects.get(id=pk)

            props = ["style_name", "color", "fabric"]
            for prop in props:
                if prop in content:
                    setattr(hat, prop, content[prop])
            hat.save()
            Hat.objects.filter(id=pk).update(**content)
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Hat does not exist"})
            response.status_code = 404
            return response
