import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList'
import HatForm from './HatForm'
import ShoeForm from './ShoeForm';
import ShoesList from './ShoesList';

function App(props) {
  if (props.hats === undefined) {
    return null;
  }
  console.log(props)
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatsList hats={props.hats} />} />
          <Route path="/hats/new" element={<HatForm />} />
          <Route path="/shoes" element={<ShoeForm />} />
          <Route path="/shoesList" element={<ShoesList shoes={props.shoes } />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
