import React, { useState, useEffect } from 'react';

function HatForm() {

    const [locations, setLocations] = useState([]);

    const [style_name, setStyleName] = useState('');
    const [fabric, setFabric] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [location, setLocation] = useState('');



    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    };

    useEffect(() => {
        fetchData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.style_name = style_name;
        data.fabric = fabric;
        data.color = color;
        data.picture = picture;
        data.location = location;
        console.log(data);

        const locationURL = 'http://localhost:8090/api/hats/';

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const response = await fetch(locationURL, fetchConfig);

        if (response.ok) {
            setStyleName('');
            setFabric('');
            setColor('');
            setPicture('');
            setLocation('');
        }
    }

    const handleStyleNameChange = async (event) => {
        const value = event.target.value;
        setStyleName(value);
    }

    const handleFabricChange = async (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleColorChange = async (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePictureChange = async (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    const handleLocationChange = async (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new hat</h1>
              <form onSubmit={handleSubmit} id="create-hat-form">

                <div className="form-floating mb-3">
                  <input value={style_name} onChange={handleStyleNameChange} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" />
                  <label htmlFor="style_name">Style Name</label>
                </div>

                <div className="form-floating mb-3">
                  <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                  <label htmlFor="fabric">Fabric</label>
                </div>

                <div className="form-floating mb-3">
                  <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="color">Color</label>
                </div>

                <div className="form-floating mb-3">
                  <input value={picture} onChange={handlePictureChange} placeholder="Picture" required type="url" name="picture" id="picture" className="form-control" />
                  <label htmlFor="picture">Picture</label>
                </div>

                <div className="mb-3">
                  <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                      return (
                        <option key={location.id} value={location.id}>
                          {location.closet_name}
                        </option>
                      );
                    })}
                  </select>
                </div>

                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
}

export default HatForm;
