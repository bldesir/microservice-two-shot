import React, { useEffect, useState } from "react";

function ShoeForm() {
  const [bin, setBin] = useState("");
  const [name, setModelName] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [color, setColor] = useState("");
  const [picture_url, setPictureUrl] = useState("");

  const [bins, setBins] = useState([]);

  const fetchData = async () => {
    const binUrl = `http://localhost:8100/api/bins/`;
    const binResponse = await fetch(binUrl);

    if (binResponse.ok) {
      const data = await binResponse.json();
      setBins(data.bins);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.manufacturer = manufacturer;
    data.model_name = name;
    data.color = color;
    data.picture_url = picture_url;
    data.bin = bin;

    console.log(data);

    const shoeUrl = "http://localhost:8080/api/shoes/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-type": "application/json",
      },
    };

    const shoeResponse = await fetch(shoeUrl, fetchConfig);
    if (shoeResponse.ok) {
      const newShoe = await shoeResponse.json();
      console.log(newShoe);

      setModelName("");
      setManufacturer("");
      setColor("");
      setPictureUrl("");
      setBin("");
    }
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setModelName(value);
  };

  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  };
  const handleBinChange = (event) => {
    const value = event.target.value;
    setBin(value);
  };

  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  };

  const handlePictureChange = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a shoe</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input
                value={name}
                onChange={handleNameChange}
                placeholder="Name"
                required
                type="text"
                name="model_name"
                id="model_name"
                className="form-control"
              />
              <label htmlFor="model_name">Shoe</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={manufacturer}
                onChange={handleManufacturerChange}
                placeholder="manufacturer"
                required
                type="text"
                name="manufacturer"
                id="manufacturer"
                className="form-control"
              />
              <label htmlFor="manufacturer">manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={color}
                onChange={handleColorChange}
                placeholder="color name"
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">color</label>
            </div>

            <div className="mb-3">
              <label htmlFor="picture_url">picture_url</label>
              <input
                value={picture_url}
                onChange={handlePictureChange}
                required
                type="url"
                className="form-control"
                id="picture_url"
                name="picture_url"
              ></input>
            </div>
            <div className="mb-3">
              <select
                value={bin}
                onChange={handleBinChange}
                required
                name="bins"
                id="bins"
                className="form-select"
              >
                <option value="">Choose a bin</option>
                {bins.map((bin) => {
                  return (
                    <option key={bin.id} value={bin.id}>
                      {bin.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
export default ShoeForm;
