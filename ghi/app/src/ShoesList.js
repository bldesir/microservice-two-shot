import { useState, useEffect } from "react"

function ShoesList(props) {

    const [shoes, setShoes] = useState([])
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model Name</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Bin</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {shoes && shoes.map(shoe => {
                        return (
                            <tr key={shoe.href}>
                                <td>{shoe.manufacturer}</td>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td>
                                    <img src={shoe.picture_url} alt="" width="100px" height="100px"/>
                                </td>

                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default ShoesList;
